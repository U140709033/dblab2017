#initial  time: 0.922 sec
# after  index: 0.5
# after PK:0.391

use mydb2;
#delete from proteins;
#set global innodb_purge_batch_size = 5000
select *from proteins ;

load data local infile 'C:\\insert.txt' into table proteins fields terminated by'|';


select*
from proteins
where protein_name like "%tumor%" and uniprot_id like "%human%"
order by uniprot_id;


create index uniprot_index on proteins (uniprot_id);
drop index uniprot_index on proteins;


alter table proteins add constraint pk_proteins primary key(uniprot_id);
alter table proteins drop primary key;